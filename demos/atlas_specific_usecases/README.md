# Training a model

An easy way to organize one's experiment is to create a folder for it, containing a `param_conf.py` and a `data_conf.py`, specifying the hyperparameters and data to be used in that experiment.
`exp_scalars/` is an example of such a folder, which creates a model that has been found to work well for processing images along with some chosen scalars.
Please see the contents of `data_conf.py` and `param_conf.py`, along with `train_model.py`, to see how everything fits together.

Run the experiment by executing e.g.
```bash
cd train_model
python train_model.py --exp_dir ./exp_scalars/ --data_path /path/to/data.h5 --rm_bad_reco True --zee_only True --n_train 1e2 --n_val 1e2 --n_test 1e2 -g 0
```

* `--exp_dir` should be a folder containing three .py files: `__init__.py`, `data_params.py` and `param_conf.py`.
* `--data_path` is the path of the data in HDF5 format. CERN users can find such datasets at the CERNBox of Lukas Ehrke, at `/eos/user/l/lehrke/Data/Data/2019-09-26/`.
* `--rm_bad_reco` removes all points which the current energy calibration reconstructs worse than 50% off (see `deepcalo.utils.make_mask`)
* `--zee_only` removes all points which are not Z->ee electrons (see `deepcalo.utils.make_mask`), which is the only thing the provided datasets support at the moment.
* `--n_train 1e2 --n_val 1e2 --n_test 1e2` means that only the first 100 points from each set will be loaded. Remove these flags to load everything.
* `-g 0` uses the first available GPU. Remove this flag to use CPU(s).

See [the DeepCalo README](https://gitlab.com/ffaye/deepcalo/blob/master/README.md) for documentation for the ModelContainer class used in `train_model.py`.



## Training a recommended models

The folder `train_recommended_models` contains a short walkthrough - `recipe.pdf` - of how the author would recommend to construct and train models for eletron and photon energy regression. It also includes a description of the image production process, which was crucial for good performance, as well as some plots showing the expected performance of the models.
This recipe is based on the thesis from October 2019 also found in the same folder.

Also contained in the folder are scripts for training the recommended models. Although the recommended data is not available at the time of writing, one should be able to use one of the datasets found at `/eos/user/l/lehrke/Data/Data/2019-09-26/` to run the following, which will construct and train the recommended models:
```bash
cd train_model
python train_model.py --exp_dir ./train_recommended_models/exp_electrons/ --data_path /path/to/data.h5 --rm_bad_reco True
```
for electrons, and
```bash
cd train_model
python train_model.py --exp_dir ./train_recommended_models/exp_photons/ --data_path /path/to/data.h5 --rm_bad_reco True
```
for photons. For the latter, you can add the flags `--rm_conv` or `--rm_unconv` to remove all respectively converted or unconverted photons from the training and validation sets.

If you do not wish to use the Track net processing the track vectors of pile-up tracks, simply comment out the `'track_names'` item of the `data_conf.py` in the experiment folder in question. Note that the `'img_names'` item of the `data_conf.py` should correspond to the name of the dataset in the `/path/to/data.h5` file that contains the added barrel and endcap images.


# Loading a model

## Basic example
Load an energy regression model that has been trained on images made from the four ECAL layers (presampler + layer 1,2,3) for simulated Z->ee electrons in the barrel:

```bash
cd use_trained_model
python load_model_basic.py --data_path /path/to/data.h5 --model_path models/model_ER_barrel_zee_electrons_images/model.json --weights_path models/model_ER_barrel_zee_electrons_images/weights.hdf5
```

## Advanced example
Load an energy regression model that has been trained on both scalar variables and images made from the four ECAL layers (presampler + layer 1,2,3) for simulated central photons (both unconverted and converted together):

```bash
cd use_trained_model
python load_model_advanced.py --data_path /path/to/data.h5 --model_path models/model_ER_central_0-100GeV_photons_images_scalars/saved_models/model.json --weights_path models/model_ER_central_0-100GeV_photons_images_scalars/saved_models/weights.hdf5
```

This does the same as the basic example, but this model uses a custom FiLM layer to process scalar variables. The primary differences are in loading and standardizing these scalars, as well as providing a FiLM class instance when loading the model architecture, so it is not *that* much more advanced than the basic example.

A compact and streamlined function for loading a model and using it for inference can be found in the form of `use_trained_model/inference.py`.



*Note that the above scripts possibly only work on Linux machines, due to the places where data or other objects are loaded.*
