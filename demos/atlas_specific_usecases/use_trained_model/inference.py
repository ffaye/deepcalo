import numpy as np
import json
from keras.models import model_from_json
import joblib


def inference(model_json_path, model_weights_path, custom_objects=None,
              images=None, scalars=None, tracks=None, multiply_output_with=None,
              images_scaler_paths=None, scalars_scaler_paths=None,
              tracks_scaler_paths=None, multiply_output_with_scaler_paths=None,
              verbose=True):
    """
    Function taking a model and some input data, and returning the model's
    predictions.

    This function is just a convenience function consisting of a series of calls
    to other functions (defined below) that actually do all the heavy lifting.

    Args:
    -----
        model_json_path : *str*
            Path to the JSON definining the model architecture.

        model_weights_path : *str*
            Path to the weights of the model. Should be in HDF5 format.

        custom_objects : *dict*
            If any custom classes (e.g. the FiLM layer) has been used in the
            model architecture, these should be provided here in the form of a
            dictionary with each key being a name of the custom object (as in
            the model JSON) with the corresponding item being an instance of the
            custom object. For example, in case of FiLM the dictionary
            `{'FiLM': deepcalo.layers.FiLM()}` should be passed.

        images : *ndarray or None*
            Numpy array of images. Should be the same shape and format as what
            the model was trained on, typically
            `(n_points, width, height, n_channels)`.
            Set `verbose=True` for a model summary that shows the
            the formats that the model expects (provided that it loads
            successfully).

        scalars : *ndarray or None*
            Numpy array of scalars with shape `(n_points, n_scalars)`. The order
            of the scalars should be the same as what the model was trained on.
            This order is the same as the order of the `'scalar_names'` found in
            the `dataparams.pkl` associated with the model.
            Set `verbose=True` for a model summary that shows the
            the formats that the model expects (provided that it loads
            successfully).

        tracks : *ndarray or None*
            Numpy array of track vectors with shape
            `(n_points, max_tracks, n_features)`. Note that the number of track
            vectors associated with each event can differ. For this reason the
            track vectors have been zero-padded (with
            `keras.preprocessing.sequence.pad_sequences`), and the `tracks`
            input should be padded with the same `maxlen` (here `max_tracks`).
            Set `verbose=True` for a model summary that shows the
            the formats that the model expects (provided that it loads
            successfully).

        multiply_output_with : *ndarray or None*
            Numpy array of containing a single scalar variable of shape
            `(n_points,)` that the output neuron of the model should be
            multiplied with. This product is then the final output of the model.
            A typical use case in energy regression is to give the accordion
            energy, such that the model predicts the ratio of the energy to the
            accordion energy.
            In DeepCalo>=0.2.1, this is integrated into models and is an input
            that the model expects. Set `verbose=True` for a model summary that
            shows the the formats that the model expects (provided that it loads
            successfully).

        <input_name>_scaler_paths : *str or list of strs or None*
            List of paths to scikit-learn-like scalers saved with joblib for
            standardization of `<input_name>` data. Anything but when
            `<input_name>` is `scalars` can be considered experimental, and will
            probably need some tweaks to this code. When `<input_name>` is
            `scalars` it is assumed that the `i`th scaler path corresponds to
            the scaler that is supposed to scale the `i`th scalar variable, i.e.
            `scalars[:,i]`.
            If only a single path is provided, it is assumed that that scaler
            will be able to transform the whole `<input_name>` data in one go.
            Considering that the track vectors have most likely been
            standardized before any zero-padding, their associated scalars will
            also need to be applied to their non-zero-padded form. For this
            reason, it is advised to not standardize `tracks` through this
            function; alternatively, one could rewrite this function to take in
            the non-padded tracks (the `maxlen` needed for zero-padding could
            probably be inferred from the model architecture).

        verbose : *bool*
            Verbose output.

    Returns:
    --------
        preds : *ndarray*
            Numpy array of predictions.
    """

    # Load model
    model = load_model(model_json_path, model_weights_path, custom_objects, verbose)

    # Organize data
    data = organize_data(images, scalars, tracks, multiply_output_with,
                         images_scaler_paths, scalars_scaler_paths,
                         tracks_scaler_paths, multiply_output_with_scaler_paths,
                         verbose)

    # Predict
    return model.predict(data).flatten()


def load_model(model_json_path, model_weights_path, custom_objects=None, verbose=True):

    # Model reconstruction from JSON
    # If custom objects have been used in the model architecture, pass them
    with open(model_json_path, 'r') as model_json:
        arch = json.load(model_json)
        model = model_from_json(arch, custom_objects=custom_objects)
    if verbose:
        print(f'Model architecture loaded from {model_json_path}.')

    # Inspect the structure of the model
    if verbose:
        model.summary()

    # Load weights
    model.load_weights(model_weights_path)
    if verbose:
        print(f'Weights loaded from {model_weights_path}.')

    return model


def organize_data(images=None, scalars=None, tracks=None, multiply_output_with=None,
                  images_scaler_paths=None, scalars_scaler_paths=None,
                  tracks_scaler_paths=None, multiply_output_with_scaler_paths=None,
                  verbose=True):

    def _report_minmax(array, name):
        print(f'Min and max of {name} after standardization: '
              f'{array.min(), array.max()}')

    x = []

    # Add images
    if images is not None:
        if type(images)==list:
            for i,img in enumerate(images):
                if images_scaler_paths is not None:
                    img = load_and_apply_scaler(images_scaler_paths[i], img)
                    if verbose:
                        _report_minmax(img, f'images')
                x.append(img)
        else:
            if images_scaler_paths is not None:
                images = load_and_apply_scaler(images_scaler_paths, images)
                if verbose:
                    _report_minmax(images, f'images')
            x.append(images)

    # Add scalars
    if scalars is not None:
        if scalars_scaler_paths is not None:
            if scalars.ndim < 2: # If there is only one scaler
                scalars = load_and_apply_scaler(scalars_scaler_paths, scalars)
                if verbose:
                    _report_minmax(scalars, 'scalar')
            else: # If there are multiple scalars
                for i in range(scalars.shape[1]):
                    scalars[:,i] = load_and_apply_scaler(scalars_scaler_paths[i], scalars[:,i])
                    if verbose:
                        _report_minmax(scalars[:,i], f'scalar {i}')
        x.append(scalars)

    # Add track vectors
    if tracks is not None:
        if tracks_scaler_paths is not None:
            if tracks.ndim < 3: # If there is only one track variable per vector
                tracks = load_and_apply_scaler(tracks_scaler_paths, tracks)
                if verbose:
                    _report_minmax(tracks, 'track variable')
            else:
                for i in range(tracks.shape[2]):
                    tracks[:,:,i] = load_and_apply_scaler(tracks_scaler_paths[i], tracks[:,:,i])
                    if verbose:
                        _report_minmax(tracks[:,:,i], f'track variable {i}')
        x.append(tracks)

    # Add multiply_output_with
    if multiply_output_with is not None:
        if multiply_output_with_scaler_paths is not None:
            multiply_output_with = load_and_apply_scaler(multiply_output_with_scaler_paths, multiply_output_with)
            if verbose:
                _report_minmax(multiply_output_with, 'multiply_output_with')
        x.append(multiply_output_with)

    return x


def load_and_apply_scaler(scaler_path, data):

    with open(scaler_path, 'rb') as f:
        scaler = joblib.load(f)

    if data.ndim < 2:
        return np.squeeze(scaler.transform(np.expand_dims(data,1)))
    else:
        return scaler.transform(data)


def set_nonpositive_preds(preds, set_value=1., verbose=True):

    preds = preds.flatten()
    n_preds = preds.shape[0]

    indices_close_to_zero = np.argwhere(np.isclose(preds,0))
    indices_negative = np.argwhere(preds < 0)

    if len(indices_close_to_zero) > 0:
        preds[indices_close_to_zero] = set_value
        print(f'WARNING: {len(indices_close_to_zero)} (out of {n_preds}) of predictions were close to zero, and have been set to {set_value}.')
    if len(indices_negative) > 0:
        preds[indices_negative] = set_value
        print(f'WARNING: {len(indices_negative)} (out of {n_preds}) of predictions were negative, and have been set to {set_value}.')

    return preds


def load_and_apply_bias_correction(preds, bc_path, bc_params):

    bc = joblib.load(bc_path)
    return bc.transform(preds.flatten(), bc_params)


if __name__=='__main__':

    import os
    import sys
    import pickle
    import argparse
    import deepcalo as dpcal
    from deepcalo.layers import FiLM

    # from packaging import version
    # assert(version.parse(dpcal.__version__) >= version.parse('0.2.1'))
    # from deepcalo.layers import BiasCorrect, BiasCorrect2D

    # Argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_path', help='Path of JSON model to be loaded.', default=None, type=str)
    parser.add_argument('--weights_path', help='Path of HDF5 weights to be loaded.', default=None, type=str)
    parser.add_argument('--data_path', help='Path to data.', default=None, type=str)
    # parser.add_argument('--bc_path', help='Path to saved bias correction class.', default=None, type=str)
    parser.add_argument('-v','--verbose', help='Verbose output, 0, 1 or 2, where 2 is less verbose than 1.', default=2, type=int)
    args = parser.parse_args()

    model_path = args.model_path
    weights_path = args.weights_path
    data_path = args.data_path
    # bc_path = args.bc_path
    verbose = args.verbose

    params_dir = 'models/model_ER_central_0-100GeV_photons_images_scalars/'

    # ==========================================================================
    # Load data to predict on
    # ==========================================================================
    # Load the first point of the training set, no points of the validation set (e.g. 'val':10), and the first 100 points of the test set.
    # Choose None instead of a number to load all points in that set (e.g. 'test':None).
    n_points = {'train':1, 'test':1e2}

    # Load data parameters associated with the model
    with open(os.path.join(params_dir,'dataparams.pkl'), 'rb') as f:
        data_params = pickle.load(f)

    data = dpcal.utils.load_atlas_data(data_path, n_points=n_points, **data_params, verbose=verbose)

    # Choose which set to predict on
    pred_set = 'test'

    # Images
    for img_name in data[pred_set]['images']:
            images = data[pred_set]['images'][img_name]
    # Scalars
    scalars = data[pred_set]['scalars']

    # Get scalers for scalars
    scaler_paths = []
    for name in data_params['scalar_names']:
        scaler_paths.append(os.path.join(params_dir, f'scalers/scaler_{name}.jbl'))

    # ==========================================================================
    # Do inference
    # ==========================================================================
    preds = inference(model_path, weights_path, custom_objects={'FiLM': FiLM()},
                      images=images, scalars=scalars, scalars_scaler_paths=scaler_paths,
                      verbose=verbose)

    if verbose:
        print('Predictions:')
        print(preds)

    # Correct unphysical values (in this case non-positive energy predictions)
    preds = set_nonpositive_preds(preds, set_value=1., verbose=verbose)

    # # ==========================================================================
    # # Correct bias
    # # ==========================================================================
    # if bc_path is not None:
    #
    #     # Define parameters that bias correction should be done as a function of
    #     bc_params = []
    #
    #     # Add eta
    #     eta = dpcal.utils.load_atlas_data(data_path, n_points=n_points,
    #                                       scalar_names=['p_eta'], verbose=verbose)[pred_set]['scalars'].flatten()
    #     bc_params.append(eta)
    #
    #     # Add log10 E_T of preds
    #     log10_et_preds = np.log10(preds * np.cosh(eta))
    #     bc_params.append(log10_et_preds)
    #
    #     preds = load_and_apply_bias_correction(preds, bc_path, bc_params)
    #
    #     # Correct unphysical values (in this case non-positive energy predictions)
    #     preds = set_nonpositive_preds(preds, set_value=1., verbose=verbose)
    #
    # if verbose:
    #     print('Predictions:')
    #     print(preds)
