def get_params():
    """
    Returns a dictionary containing parameters to be passed to the loading
    function.
    """

    params = {'target_name'            : 'p_truth_E',
              'img_names'              : ['em_barrel'], # Barrel: ['em_barrel'], Crack: ['em_barrel', 'em_endcap', 'tile_gap'], Endcap: ['em_endcap']
              'gate_img_prefix'        : None,
              'scalar_names'           : ['p_pt_track', # Set to None if you only want to use the images
                                          'p_eAccCluster',
                                          'p_cellIndexCluster'],
              'track_names'            : None,
              'max_tracks'             : None,
              'multiply_output_name'   : None,
              'sample_weight_name'     : None,
              }

    return params
